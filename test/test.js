var request = require('supertest');
var app = require('../server.js')
describe('GET /', function() {
    it('displays "Hello World!"', function(done) { // The line below is the core test of our app.  
        request(app).get('/').expect('Hello World!', done);
    });
});

/* 
In this tutorial, we've implemented a feature and then wrote a test for it.
It can be interesting to do the opposite and start by writing the tests that should verify your feature. 
Then you can implement the feature knowing that you already have safeguards to make sure that it's working as expected.
*/